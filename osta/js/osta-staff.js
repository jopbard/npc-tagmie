
// When the user scrolls down 600px from the top of the document, show the button
window.onscroll = function() {scrollFunction()};
function scrollFunction() {
  if (document.body.scrollTop > 600  || document.documentElement.scrollTop > 600) {
	document.getElementById("scroll-to-top").style.visibility = "visible";
	document.getElementById("scroll-to-top").style.opacity = "0.5";
  } else {
	document.getElementById("scroll-to-top").style.visibility = "hidden";
	document.getElementById("scroll-to-top").style.opacity = "0";
  }
}
// When the user clicks on the button, scroll to the top of the document
function topFunction() {
  document.body.scrollTop = 0;
  document.documentElement.scrollTop = 0;
}
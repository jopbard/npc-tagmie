$(document).ready(function(){
	// Show search box in Client Portal pages with sidebar
	if ( $( "a.active.kb" ).length ) {
		$( "#landing-search" ).addClass( "show" );		
		$( "#pre-footer-inner" ).addClass( "show" );		
	} else {
		$( "#landing-search" ).addClass( "hide" );
		$( "#pre-footer-inner" ).addClass( "hide" );	
	}
	if ( $( "#landing-search.show" ).length ) {	
		$( "#pre-footer-inner" ).removeClass( "show" );	
		$( "#pre-footer-inner" ).addClass( "hide" );		
	} else {
		$( "#pre-footer-inner" ).removeClass( "hide" );			
		$( "#pre-footer-inner" ).addClass( "show" );	
	}
});



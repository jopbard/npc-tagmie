<?php 
function default_config() { 
	return array( 
		"theme"=>"ice", 
		"logo-options"=>"default",
		"headerbg"=>"#22545c", 
		"headertitlecolor"=>"#ffffff", 
		"navbarbg"=>"#89c4c6", 
		"navbarlink"=>"#22545c",
		"mobilemenubg"=>"#89c4c6", 
		"mobilelinkcolor"=>"#22545c",
		"stickybar"=>"#22545c",
		"title"=>"osTicket Awesome",
		"subtitle"=>"Support Ticket Center",
		"mobile-text"=>"osTicket Awesome",
		"mobile-link"=>"https://osticketawesome.com",
		"backdrop"=>"01",
		"upload-dir"=>  ROOT_PATH  . "osta/uploads/",
		"header-options"=> "header-solid-color", 
		"backdrop-options"=>"solid-color",
		"custom-backdrops"=>"[]",
		"custom-logos"=>"[]",
	);
};

function get_config() { 
	$config = default_config();
	$sql = "SELECT * FROM " . TABLE_PREFIX . "config WHERE `namespace`=\"osticketawesome\"";
	$res = db_query($sql);

	while(  $row = db_fetch_array($res) ) { 
		$config[ $row["key"] ] = $row["value"];
	}
	return $config;
}

function get_theme_css() { 
	ob_start();
	$config = get_config();
	$theme = $config["theme"];
	$base =  ROOT_PATH;
	?>
	<style type="text/css">
	<?php
	if ( $theme == "custom" ) { 
		$themes = array( "headerbg"=>"header-bg", "headertitlecolor"=>"header-title-color", "navbarbg"=>"nav-bar-bg", "navbarlink"=>"nav-bar-link", "mobilemenubg"=>"mobile-menu-bg", "mobilelinkcolor"=>"mobile-link-color" , "stickybar"=>"stickybar" );

		?>
			:root {
		<?php 
			foreach( $themes as $key=>$value ) { 
				echo "--" . $value . ": " . $config[$key] . ";";
			}
			?>}
		<?php
	}
	?>
	#header,
	#loginBody #brickwall,
	#background-solid-image .image	{
        background-image: url("<?php echo $base; ?>osta/img/backdrops/<?php echo $config["backdrop"]?>");
	}
	<?php
	if ( $config[ "header-options" ] == "header-solid-color" ) {
		?>
		#header {
		   background-image: initial !important;
		}
		<?php
	}
	?>
	<?php
	if ( $config[ "backdrop-options" ] == "solid-color" ) {
		?>
		#loginBody #brickwall {
			background-image: initial;
		}
		<?php
	}
	?>	
	</style>

	<link type="text/css" rel="stylesheet" href="<?php echo $base; ?>osta/opt/logo/logo-options-<?php  echo $config["logo-options"]  ?>.css">
	<link rel="shortcut icon" href="<?php echo $base; ?>osta/css/themes/<?php echo $theme ?>/favicon.ico">
	<link rel="shortcut icon" href="<?php echo $base; ?>osta/css/themes/<?php echo $theme ?>/favicon.png">
	

	<?php
	//if ( $theme != "custom" ) 
		echo  "<link type=\"text/css\" rel=\"stylesheet\" id=\"jssDefault\" href=\"" . $base . "osta/css/themes/" . $theme . ".css\">";
	return ob_get_clean();
}

function chk($config,$key,$value) { 
	return ($config[$key] == $value ) ? " checked=\"checked\"" : "";
}

function notchk($config,$key,$value) { 
	return ($config[$key] != $value ) ? " checked=\"checked\"" : "";
}

function uploadImage($file, &$error, $upload_dir, $image_array, $aspect_ratio=2) {
      
        if (extension_loaded('gd')) {
            $source_path = $file['tmp_name'];
            list($source_width, $source_height, $source_type) = getimagesize($source_path);

            switch ($source_type) {
                case IMAGETYPE_GIF:
                case IMAGETYPE_JPEG:
                case IMAGETYPE_PNG:
                    break;
                default:
                    $error = __('Invalid image file type');
                    return false;
            }
        }

        if(!$file['name'] || $file['error'] || !is_uploaded_file($file['tmp_name'])) {
        	$error = __('Invalid Upload');
            return false;
        }

        list($key, $sig) = AttachmentFile::_getKeyAndHash($file['tmp_name'], true);

        $info=array('type'=>$file['type'],
                    'filetype'=>$ft,
                    'size'=>$file['size'],
                    'name'=>$file['name'],
                    'key'=>$key,
                    'signature'=>$sig,
                    'tmp_name'=>$file['tmp_name'],
                    );

        if ( move_uploaded_file( $info["tmp_name"], $_SERVER['DOCUMENT_ROOT'] . $upload_dir . $info["name"] ) ) 
		{
			$a = json_decode( $image_array , true);
			$a[] = array( "client"=>false, "staff"=>false, "image"=>$info["name"] );
			return json_encode($a);
		}
		$error = "Unable to write to:" .  $upload_dir . $info["name"] ;
		return false;
    }

function deleteImages($images, $deletedKeys, $upload_dir ) {
	$images = json_decode( $images, true );
	$out = [];
	foreach( $images as $k=>$v ) { 
		if ( in_array( $k, $deletedKeys ) ) { 
			unlink( $_SERVER['DOCUMENT_ROOT'] . $upload_dir . $v["image"] );
		}
		else  $out[] = $v;
	} 
	return json_encode( $out );

}

function selectImage($images, $selected, $type ) {
	$images = json_decode( $images, true );
	foreach( $images as $k=>$v ) { 
		//echo $type . " = " . $selected . " = " . $k . print_r( $v, true ) . "<BR>";
		$images[$k][$type] = $selected == $k ? true : false;
	} 
	return json_encode( $images );

}

function dbg($var) { 
	return "<pre>" . print_r( $var, true ) . "</pre>";
}

function get_logo($config, $type) {
	if( isset( $config ) && isset( $config["custom-logos"] ) ) { 
		 foreach( json_decode( $config["custom-logos"], true )  as $k=>$v ) { 
		 	if ( isset( $v[$type] ) && $v[$type] == true ) {
		 		return ( isset( $config["upload-dir"] ) ? $config["upload-dir"] : "" )  . $v["image"]; 
		 	}
		 };
	}
	return ROOT_PATH . "scp/logo.php";
}


function update_config($post) { 
	$debug = false;
	/*
	if($cfg && $cfg->updateSettings($post,$errors)) {
	    $msg=sprintf(__('Successfully updated %s.'), Format::htmlchars($page[0]));
	} elseif(!$errors['err']) {
	    $errors['err'] = sprintf('%s %s',
	        __('Unable to update settings.'),
	        __('Correct any errors below and try again.'));
	} */
	
	$config = get_config(); 
	$post = array_merge( $config, $post );
	$options = array_keys($config);
	if( $post["radio"] == "custom-theme-options" ) $post["theme"] = "custom";
	else if ( $post["theme"] == "custom" )  $post["theme"] = "ice";


	if ( isset( $post[ "selected-logo"] ) ) { 
		$post["custom-logos"] = selectImage( $post["custom-logos"], $post[ "selected-logo"], "client" );
	}

	if ( isset( $post[ "selected-logo-scp"] ) ) { 
		$post["custom-logos"] = selectImage( $post["custom-logos"], $post[ "selected-logo-scp"], "staff" );
	}
	

	if ( isset( $post[ "selected-backdrop"] ) ) { 
		$post["custom-backdrops"] = selectImage( $post["custom-backdrops"], $post[ "selected-backdrop"], "client" );
	}

	if ( isset( $post[ "selected-backdrop-scp"] ) ) { 
		$post["custom-backdrops"] = selectImage( $post["custom-backdrops"], $post[ "selected-backdrop-scp"], "staff" );
	}


	if ( isset( $post[ "delete-logo"] ) ) { 
		$post["custom-logos"] = deleteImages( $post["custom-logos"], $post[ "delete-logo"], $config["upload-dir"]);
	}

	if ( isset( $post[ "delete-backdrop"] ) ) { 
		$post["custom-backdrops"] = deleteImages( $post["custom-backdrops"], $post[ "delete-backdrop"], $config["upload-dir"]);
	}

	 if ($_FILES['logo']) {
        $error = false;
        list($logo) = AttachmentFile::format($_FILES['logo']);
        if (!$logo)
            ; 
        elseif ($logo['error'])
        	$_SESSION["errors"][] = $logo['error'];
        elseif (!$new_images= uploadImage($logo, $error, $config["upload-dir"], $post["custom-logos"], 9999999 )) // 2
        	$_SESSION["errors"][] = sprintf(__('Unable to upload logo image: %s'), $error);
        else { 
        	$post["custom-logos"] = $new_images;
        }
    }

    if ($_FILES['backdrop']) {
        $error = false;
        list($backdrop) = AttachmentFile::format($_FILES['backdrop']);
        if (!$backdrop)
            ; 
        elseif ($backdrop['error'])
        	$_SESSION["errors"][] = $backdrop['error'];
        elseif (!$new_images= uploadImage($backdrop, $error, $config["upload-dir"], $post["custom-backdrops"], 9999999 )) // 2
        	$_SESSION["errors"][] = sprintf(__('Unable to upload backdrop image: %s'), $error);
        else { 
        	$post["custom-backdrops"] = $new_images;
        }
    }


	for( $x=0; $x<sizeof( $options ); $x++ ) {
		if( $options[$x]  == "upload-dir" ) continue;
		if ( isset( $post[ $options[$x] ] ) ) { 
			$sql = "SELECT * FROM " . TABLE_PREFIX . "config WHERE `namespace`=\"osticketawesome\" and `key`=\"" . $options[$x] . "\"";
			$res = db_query($sql);
			if(  $row = db_fetch_array($res) ) { 
				$sql = "UPDATE " . TABLE_PREFIX . "config set `value` = ? where `namespace`='osticketawesome' and `key`='" . $options[$x]  . "'";
				if( $debug ) echo $sql . "<br/>";
				$stmt = db_prepare($sql);
				$stmt->bind_param("s", $post[ $options[$x] ] );
				$stmt->execute(); 
			}
			else { 
				$id_start =  77700;
				$sql = "SELECT max(id) as id  FROM `" . TABLE_PREFIX . "config` WHERE id > " . $id_start;
				$row = db_fetch_array(db_query($sql));
				if( $row["id"] == null ) $row["id"] = $id_start;
				$sql = "INSERT INTO `" . TABLE_PREFIX . "config` (`id`,`namespace`,`key`, `value` ) VALUES (" . ( $row["id"] + 1 ) . ",'osticketawesome' , ?, ?)";
				if( $debug ) echo $sql . "<br/>";
				$stmt = db_prepare($sql);
				$stmt->bind_param("ss",  $options[$x] , $post[ $options[$x] ] );
				$stmt->execute(); 
			}
		}

	}	
	if( $debug ) {
		echo print_r( $post, true );
		echo print_r( $_FILES, true );

		die();
	} 

}


?>
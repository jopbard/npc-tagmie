        </div>
    </div>
    <!-- osta -->
    <div class="clear"></div>
    
<?php
	if ($cfg && $cfg->isKnowledgebaseEnabled()) { ?>
	<div id="pre-footer">
		<div id="pre-footer-inner" class="toggle">
			<div class="searchArea">
				<form method="get" action="kb/faq.php">
				<button type="submit" tabindex="2">
					<div class="client-choice-icon">
						<svg style="width:24px;height:24px" viewBox="0 0 24 24">
							<path d="M9.5,3A6.5,6.5 0 0,1 16,9.5C16,11.11 15.41,12.59 14.44,13.73L14.71,14H15.5L20.5,19L19,20.5L14,15.5V14.71L13.73,14.44C12.59,15.41 11.11,16 9.5,16A6.5,6.5 0 0,1 3,9.5A6.5,6.5 0 0,1 9.5,3M9.5,5C7,5 5,7 5,9.5C5,12 7,14 9.5,14C12,14 14,12 14,9.5C14,7 12,5 9.5,5Z" />
						</svg>
					</div>				
				<?php echo __('Search'); ?></button>
				<div class="inputDiv">
					<input type="hidden" name="a" value="search"/>
					<input type="text" name="q" class="search" placeholder="<?php echo __('Search our knowledge base'); ?>"/>
				</div>
			</div>				
		</div>
		<div class="clear"></div>
	</div> 		
<?php
	}
		else
			echo  '';
		?>

	<div class="clear"></div>
    <div id="footer"><!-- osta -->
		<?php include ROOT_DIR . 'osta/inc/client-foot.html'; ?>   
    </div>
	<!--osta-->
	<div id="overlay"></div>
	<div id="loading">
		<!-- <i class="icon-spinner icon-spin icon-3x pull-left icon-light"></i>
		<h1><?php //echo __('Loading ...');?></h1> -->
		<img style="text-align: center;" src="http://tagmie.com/images/ballswing.gif" alt="..."/>
	</div>
	<?php
	if (($lang = Internationalization::getCurrentLanguage()) && $lang != 'en_US') { ?>
		<script type="text/javascript" src="ajax.php/i18n/<?php
			echo $lang; ?>/js"></script>
	<?php } ?>
	<script type="text/javascript">
		getConfig().resolve(<?php
			include INCLUDE_DIR . 'ajax.config.php';
			$api = new ConfigAjaxAPI();
			print $api->client(false);
		?>);
	</script>
	<script>
	// var f = ['🌑', '🌘', '🌗', '🌖', '🌕', '🌔', '🌓', '🌒'],
    //     d = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    //     m = 0;

    // function loop() {
    //     var s = '', x = 0;

    //     if (!m) {
    //         while (d[x] == 4) {
    //             x ++;
    //         }

    //         if (x >= d.length) m = 1;
    //         else {
    //             d[x] ++;
    //         }
    //     }
    //     else {
    //         while (d[x] == 0) {
    //             x ++;
    //         }

    //         if (x >= d.length) m = 0;
    //         else {
    //             d[x] ++;

    //             if (d[x] == 8) d[x] = 0;
    //         }
    //     }

    //     d.forEach(function (n) {
    //         s += f[n];
    //     });

    //     location.hash = s;

    //     setTimeout(loop, 50);
    // }

    // loop();

		// var l = ["▁", "▂", "▃", "▄", "▅", "▆", "▇", "█"];
		// var x = new AudioContext();
		// var a = x.createAnalyser();
		// a.fftSize = 32;
		// var d = new Uint8Array(16);
		// navigator.mediaDevices.getUserMedia({ audio: true }).then(s => {
		// 	x.createMediaStreamSource(s).connect(a);
		// 	z();
		// });

		// function z() {
		// 	setTimeout(z, 40);
		// 	a.getByteFrequencyData(d);
		// 	var s = [];
		// 	d.forEach(v => s.push(l[Math.floor((v / 255) * 8)]));
		// 	var t = document.title = s.join("");
		// 	history.replaceState(history.state, t, '#' + t);
		// }
	</script>
<?php include ROOT_DIR . 'osta/inc/back-button.html'; ?> 
<?php include ROOT_DIR . 'osta/inc/database-reset-warning.html'; ?>
<?php include ROOT_DIR . 'osta/inc/theme-button.html'; ?>    
</body>
</html>

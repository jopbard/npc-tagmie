<html>

<head>
    <style type="text/css">
@page {
    header: html_def;
    footer: html_def;
    margin: 15mm;
    margin-top: 30mm;
    margin-bottom: 22mm;
}
.logo {
  max-width: 220px;
  max-height: 71px;
  width: auto;
  height: auto;
  margin: 0;
}
#ticket_thread .message,
#ticket_thread .response,
#ticket_thread .note {
    margin-top:10px;
    border:1px solid #aaa;
    border-bottom:2px solid #aaa;
}
#ticket_thread .header {
    text-align:left;
    border-bottom:1px solid #aaa;
    padding:3px;
    width: 100%;
    table-layout: fixed;
}
#ticket_thread .message .header {
    background:#C3D9FF;
}
#ticket_thread .response .header {
    background:#FFE0B3;
}
#ticket_thread .note .header {
    background:#FFE;
}
#ticket_thread .info {
    padding:5px;
    background: snow;
    border-top: 0.3mm solid #ccc;
}
table.meta-data {
    width: 100%;
}
table.custom-data {
    margin-top: 10px;
}
table.custom-data th {
    width: 25%;
}
table.custom-data th,
table.meta-data th {
    text-align: right;
    background-color: #ddd;
    padding: 3px 8px;
}
table.meta-data td {
    padding: 3px 8px;
}
.faded {
    color:#666;
}
.pull-left {
    float: left;
}
.pull-right {
    float: right;
}
.flush-right {
    text-align: right;
}
.flush-left {
    text-align: left;
}
.ltr {
    direction: ltr;
    unicode-bidi: embed;
}
.headline {
    border-bottom: 2px solid black;
    font-weight: bold;
}
div.hr {
    border-top: 0.2mm solid #bbb;
    margin: 0.5mm 0;
    font-size: 0.0001em;
}
.thread-entry, .thread-body {
    page-break-inside: avoid;
}

/* 09/10/19  */
.box-border {
  border: 1px solid black;
}

.text-title {
  font-size: 15px;
}

.flush-center {
  text-align: center;
}

.text-bold {
  font-weight: bold;
}

.bordered-bottom {
  border-bottom: 2px solid black;
}

.table-padding-left {
  padding-left: 30px;
}

.text-hint {
    font-size: 10px;
}

.bg-black {
  background-color: black;
}

td {
  padding-bottom: 10px;
}
<?php include ROOT_DIR . 'css/thread.css'; ?>
    </style>
</head>
<body>


<htmlpageheader name="def" style="display:none">
    <table width="100%" cellpadding="0" cellspacing="0">
    <tr>
      <td class="flush-right" style="padding-bottom: 0px;">
        Complaints-Assisted Form
      </td>
    </tr>
    <tr>
      <td class="flush-right" style="padding-bottom: 0px;">
          Page {PAGENO} of {nb}
      </td>
    </tr>
  </table>
  <div class="hr">&nbsp;</div>
</htmlpageheader>

<!-- Ticket metadata -->
<div class="flush-center"><img style="height: 160px;" src="http://tagmie.com/images/npclogo.png"/><br><span class="text-title text-bold">COMPLAINTS-ASSISTED FORM</span></div>


<p>By filling out this complaint form, you hereby sworn that you are the complainant and you will have to appear
in the office ofr oath taking. Willful and deliberate assertion of falsehood will be criminally prosecuted. List
of accepted government issued ID's: </p>

<ul>
  <li>Philippine Passport</li>
  <li>Philippine Driver's License</li>
  <li>PRC ID</li>
  <li>Postal ID</li>
  <li>Voter's ID</li>
  <li>GSIS Card</li>
  <li>SSS Card</li>
  <li>TIN Card</li>
  <li>Student ID</li>
</ul>

<p class="text-bold">Do you have a concern about a privacy violation, personal data breach or matters related to personal data protection,
  or any other violation of the Data Privacy Act, its IRR and other issuances?</p>


<table width="100%" cellpadding="0" cellspacing="0">
  <tbody>
    <tr>
      <td width="5%" class="box-border bg-black"></td>
      <td width="95%" class="table-padding-left">YES. Proceed to the next section.</td>
    </tr>

    <tr>
       <td width="(100/1)%" colspan="2" height="26"></td>
    </tr>

    <tr>
      <td width="5%" class="box-border"></td>
      <td width="95%" class="table-padding-left" rowspan="2">If NO, do not proceed. The National Privacy Commission may have no power to act on your complaint.
        The Commission can only act on matters that relate to Data Privacy.</td>
    </tr>

    <tr>
      <td></td>
      <td></td>
    </tr>

    <tr>
       <td width="(100/1)%" colspan="2" height="26"></td>
    </tr>

    <tr>
      <!-- cellspan  -->
      <td class="text-bold" width="(100/1)%" colspan="2">Does your concern affect you personally or it involves your personal data?</td>
    </tr>

    <tr>
       <td width="(100/1)%" colspan="2" height="26"></td>
    </tr>

    <tr>
      <td width="5%" class="box-border bg-black"></td>
      <td width="95%" class="table-padding-left">YES, Proceed.</td>
    </tr>

    <tr>
       <td width="(100/1)%" colspan="2" height="26"></td>
    </tr>

    <tr>
      <td width="5%" class="box-border"></td>
      <td width="95%" class="table-padding-left">NO.</td>
    </tr>

    <!-- START: whitespace -->
    <tr>
       <td width="(100/1)%" colspan="2" height="26"></td>
    </tr>
    <!-- END: whitespace -->

    <tr>
      <!-- cellspan -->
      <td class="text-bold" width="(100/1)%" colspan="2">Have you contacted the agency/individual you are about to complain?</td>
    </tr>

    <!-- START: whitespace -->
    <tr>
       <td width="(100/1)%" colspan="2" height="26"></td>
    </tr>
    <!-- END: whitespace -->

    <tr>
      <td width="5%" class="box-border"></td>
      <td width="95%" class="table-padding-left" >YES, Please attach proof.</td>
    </tr>

    <!-- START: whitespace -->
    <tr>
       <td width="(100/1)%" colspan="2" height="26"></td>
    </tr>
    <!-- END: whitespace -->

    <tr>
      <td width="5%" class="box-border bg-black"></td>
      <td width="95%" class="table-padding-left" >NO. Please explain</td>
    </tr>

    <tr>
      <td width="5%"></td>
      <td width="95%" height="100%" class="table-padding-left"></td>
    </tr>
    <tr>
      <td width="5%"></td>
      <td width="95%" class="table-padding-left" rowspan="3">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus efficitur, leo eu rhoncus egestas, velit magna rutrum tortor, sed tincidunt eros orci et nulla. Nam ac velit eu nulla condimentum vehicula id elementum orci. Sed massa leo, dictum ac scelerisque ultricies, viverra quis mauris. Vivamus semper tortor rhoncus, euismod orci quis, mollis nisl. Maecenas ac lorem consectetur, dapibus lacus eu, consequat quam. Vestibulum viverra maximus orci, a sollicitudin ante lobortis id. Nunc condimentum turpis ante, sit amet ornare neque rutrum non. Ut ornare tortor vitae magna rhoncus porta</td>
    </tr>
    <tr>
      <td></td>
      <td></td>
    </tr>

    <tr>
      <td></td>
      <td></td>
    </tr>


  </tbody>
</table>

<p class="text-bold">Complainant's Personal Information</p>

<table width="100%">
  <tr>
    <td class="text-bold" width="25%" valign="top">First Name</td>
    <td width="75%">Rodrigo</td>
  </tr>
  <tr>
    <td class="text-bold" width="25%" valign="top">Middle Initial</td>
    <td width="75%">R</td>
  </tr>
  <tr>
    <td class="text-bold" width="25%" valign="top">Last Name</td>
    <td width="75%">Duterte</td>
  </tr>
  <tr>
    <td class="text-bold" width="25%" valign="top">Valid ID</td>
    <td width="75%">Postal ID</td>
  </tr>
  <tr>
    <td class="text-bold" width="25%" valign="top">ID Number</td>
    <td width="75%">232323</td>
  </tr>
  <tr >
    <td height="40px" width="25%" valign="top"><span class="text-bold">Present Address</span>
    <br><span class="text-hint">*Please indicate complete address, including zip code, for mailing of orders and other issuances of this Commissions</span></td>
    <td width="75%">Blk 7 Lot 48 Harmony Hills 1, SJDM, Bulacan, 234, Philippines</td>
  </tr>
  

  <tr >
    <td height="40px" width="25%" valign="top"><span class="text-bold">Permanent Address</span>
    <br><span class="text-hint">*Please indicate complete address, including zip code, for mailing of orders and other issuances of this Commissions</span></td>
    <td width="75%">Somewhere Street, Over the Rainbow Village, South Atlantis 3454</td>
  </tr>

  <tr>
    <td class="text-bold" width="25%" valign="top">Email Address</td>
    <td width="75%">RD@gov.ph</td>
  </tr>
  <tr>
    <td class="text-bold" width="25%" valign="top">Contact Number</td>
    <td width="75%">#455544554</td>
  </tr>
</table>

<p class="text-bold">Respondent's Infomation (Party Complainted Against)</p>

<table width="100%">
  <tr>
    <td class="text-bold" width="25%" valign="top">Name</td>
    <td width="75%">Xi Jin Ping</td>
  </tr>
  <tr >
    <td height="40px" width="25%" valign="top"><span class="text-bold">Present Address</span>
    <br><span class="text-hint">*Please indicate complete address, including zip code, for mailing of orders and other issuances of this Commissions</span></td>
    <td width="75%">69 Ching Chong Street, Butchekik City, 3445, China</td>
  </tr>

  <tr>
    <td class="text-bold" width="25%" valign="top">Contact Number</td> 
    <td width="75%">455555</td>
  </tr>
  <tr>
    <td class="text-bold" width="25%" valign="top">Website (if any)</td>
    <td width="75%">chingchang.cn</td>
  </tr>
</table>

<p class="text-bold">Complaint</p>
<table>
  <tr>
    <td class="text-bold" width="25%">Description</td>
    <td width="75%"></td>
  </tr>
  <tr>
    <td width="100" colspan="2">
      Lorem ipsum dolor sit amet, ccenas enlus. Nulla pelulla id quaPhasellus rutrum eleifend malesuada. Pellentesque ultrices enim facilisis, blandit arcu in, pulvinar mauris. Proin luctus augue et odio lacinia, vitae aliquet mi ultrices. Praesent pharetra scelerisque enim, vitae lacinia nisl placerat nec. Phasellus tempor felis ut porttitor molestie. Fusce scelerisque sollicitudin quam. Suspendisse facilisis nunc nec nunc posuere ultricies. Nam vitae rutrum ligula, non ullamcorper neque. Mauris egestas in felis nec aliquet. Aenean gravida, dolor et fringilla sollicitudin, lectus ex dictum leo, faucibus maximus augue metus a tellus. Suspendisse at vehicula lacus.
    </td>
  </tr>
  <tr>
    <td class="text-bold" width="25%" valign="top">Provision violated by the respondent.</td>
    <td width="75%">Section 25 (Unauthorized Processing of Personal Infomation and Sensitive
        Personal Information)</td>
  </tr>
  <tr> 
      <td width="25%" class="text-bold" valign="top">What personal information about your was affected?</td>
      <td width="75%">
        Quisque auctor magna ac nulla tempus laoreet. Integer vehicula risus ut risus finibus, vel molestie ipsum dictum. Suspendisse dui ex, vestibulum vitae odio eu, pretium consequat velit. Vestibulum id nisi nec nibh imperdiet semper. Aenean tincidunt nisl eu sem sagittis, at malesuada lacus ultrices. Morbi quis nunc semper, elementum massa eget, dapibus quam. Phasellus pulvinar sed eros vel tristique. Phasellus gravida orci tempus lectus sagittis vehicula. Donec commodo iaculis mi id laoreet. Aliquam bibendum nisl at elementum cursus. Maecenas dui neque, suscipit vitae fermentum ac, mattis sed urna. Cras non ex a ligula consequat congue ac ut ante. Integer aliquet et nisi sit amet congue. Nulla fermentum dui eget diam bibendum maximus. Mauris blandit felis lorem, id sollicitudin nisi placerat ac. Ut dapibus congue velit, at interdum massa.
      </td>
  </tr>


</table>

<table width="100%">

    <tr>
      <td width="25%" class="text-bold" valign="top">Date and Time of Incident</td>
      <td width="75%">
        September 29, 2018 10:30am
      </td>
    </tr>
    <tr>
      <td width="25%" class="text-bold" valign="top">Place of Incident</td>
      <td width="75%">
        Makati City
      </td>
    </tr>
    <tr>
      <td width="25%" class="text-bold" valign="top">Do you know who did it?</td>
      <td width="75%">
        Yes
      </td>
    </tr>
    <tr>
      <td width="25%" class="text-bold" valign="top">How and when did you find out about it?</td>
      <td width="75%">
        Proin eu urna ut velit finibus sagittis eget id nunc. Maecenas enim lorem, aliquet nec dui aliquet, eleifend tempor tellus. Nulla pellentesque purus at luctus euismod. Nulla id quam ac nulla luctus luctus
      </td>
    </tr>
    <tr>
      <td width="25%" class="text-bold" valign="top">How have you been affected?</td>
      <td width="75%">
        Proin eu urna ut velit finibus sagittis eget id nunc. Maecenas enim lorem, aliquet nec dui aliquet, eleifend tempor tellus. Nulla pellentesque purus at luctus euismod. Nulla id quam ac nulla luctus luctus
      </td>
    </tr>
</table>
<p class="text-bold">Whay would resolve this complaint for you?</p>
<ul>
  <li>Settlement</li>
</ul>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vel accumsan velit, non aliquet nulla. Vivamus in ipsum mauris. Nam pulvinar, risus sit amet gravida lacinia, turpis ex mattis eros, quis pretium sapien elit eu dolor. Nam nisi sapien, mattis sed sapien ac, ullamcorper dictum mi. Aliquam non pretium quam</p>
<ul>
  <li>Damages</li>
</ul>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vel accumsan velit, non aliquet nulla. Vivamus in ipsum mauris. Nam pulvinar, risus sit amet gravida lacinia, turpis ex mattis eros, quis pretium sapien elit eu dolor. Nam nisi sapien, mattis sed sapien ac, ullamcorper dictum mi. Aliquam non pretium quam</p>

<p class="text-bold">Are you seeking an Order to temporarily or permanently stop the processing of your data?
(This requires a summary hearing.)</p>

<table>
  <tr>
    <td>YES</td>
  </tr>
  <tr>
    <td>Fusce eget urna eget tortor porttitor efficitur. Aenean id felis porttitor dolor egestas scelerisque ut a augue. Nulla sit amet magna quis lacus sagittis sagittis eu ut dui. Aenean eu diam vel ligula gravida condimentum ac a arcu. Nunc laoreet rhoncus eros, vitae dictum augue consequat eleifend.</td>
  </tr>
</table>


<span class="text-bold">Encoded by:</span><span> Juan Dela Cruz Jr.</span>


</body>
</html>

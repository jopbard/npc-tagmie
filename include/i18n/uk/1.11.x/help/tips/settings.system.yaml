---
helpdesk_status:
  title: Статус системи
  content: >
    If the status is changed to <span class="doc-desc-opt">Offline</span>, the client interface will be disabled.  Only Admins will be able to access the system.
helpdesk_url:
  title: URL системи
  content: >
    Цей URL є базовим для osTicket. Він буде використаний у повідомленнях електронної пошти, для направлення клієнтів до Служби підтримки.
helpdesk_name_title:
  title: Ім'я/назва системи заявок
  content: >
    Ця назва відображається на вкладці браузера - метатег заголовку. 
    Якщо URL системи доданий в закладки, то ця назва і там буде відображатися.
default_department:
  title: Відділ за замовчуванням
  content: >
    Виберіть <span class = "doc-desc-title"> відділ </span> за замовчуванням для заявок, які не направляються іншому відділу автоматично. <br/> <br/> Заявка може бути спрямована на підставі налаштувань категорії заявки, вхідної пошти та фільтра заявок.
default_page_size:
  title: Розмір сторінки за замовчуванням
  content: >
    Виберіть кількість елементів, що відображаються на сторінці в чергах заявок в панелі співробітників. Кожен співробітник може також налаштувати це число для власного облікового запису в розділі <span class = "doc-desc-title"> Мої Налаштування </span>.
default_log_level:
  title: Рівень деталізації журналу за замовчуванням
  content: >
    Визначте мінімальний рівень подій, які будуть записуватися в <span class = "doc-desc-title"> системний журнал </span>. <span class = "doc-desc-opt"> Налагодження </span> представляє найнижчий рівень серйозності, а <span class = "doc-desc-opt"> помилка </span> - найвищий. Наприклад, якщо Ви хочете бачити всі події в <span class = "doc-desc-title"> системному журналі </span> - виберіть рівень подій - <span class = "doc-desc-opt"> налагодження </span>.
purge_logs:
  title: Очистити журнали
  content: >
    Визначте, як довго будуть зберігатися записи в <span class = "doc-desc-title"> системному журналі </span>, після чого вони будуть видалені.
enable_richtext:
  title: Розширене оформлення тексту
  content: >
    Якщо включено, це дозволить використовувати додаткове форматування тексту у спілкуванні між клієнтами та співробітниками.
enable_avatars:
  title: Дозволити аватари при потоковому перегляді
  content: >
    Активуйте для відображення <span class="doc-desc-title">Аватарів</span> в потоці повідомлень.<br><br> <span class="doc-desc-title">Джерело Аватару</span> може бути встановлене на сторінках налаштувань Агента або Користувача.
  links:
    - 
      title: Налаштування користувача
      href: /scp/settings.php?t=agents
    - 
      title: Налаштування користувача
      href: /scp/settings.php?t=users
collision_avoidance:
  title: Тривалість сесії співробітника
  content: >
    Введіть максимальну тривалість часу, коли Агент утримує блокування заявки без будь-якої активності. <br> <br> Введіть <span class = "doc-desc-opt"> 0 </span>, щоб вимкнути функцію блокування.
date_time_options:
  title: Параметри дати та часу
  content: >
    Наступні налаштування визначають налаштування Дати та Часу системи підтримки користувачів за замовченням. Ви можете використвувати локалізовані налаштування або підлаштувати формати для своїх унікальних потреб. Зверніться до довідки по ICU форматах, щоб дізнатися про налаштування. Дати, відображені нижче ілюструють результат їхніх відповідних значень.
  links:
    - 
      title: Дивіться синтаксис функції PHP Date
      href: http://userguide.icu-project.org/formatparse/datetime
languages:
  title: Системна мова
  content: >
    Виберіть основну мову систему і необов'язкові другорядні мови, щоб зробити інтерфейс максимально локалізованм для ваших агентів і кінцевих-користувачів.
primary_language:
  title: Основна мова системи
  content: >
    Контент відображатиметься агентам і кінцевим користувачам цією мовою, якщо налаштована для них мова зараз недоступна. Це включає контент інтерфейсної частини а також сторінки подяки та повідомлення електронної пошти.<br/><br/> Це мова, якою повинні бути відображені неперекладені версії контенту.
secondary_language:
  title: Другорядна мова
  content: >
    Налаштуйте параметри мови для агентів та кінцевих користувачів. Інтерфейс буде доступний для цих мов і особливий контент, такий як сторінки подяки і допомога будуть доступні для цих мов.
attachments:
  title: Налаштування вкладень і сховища
  content: >
    Налаштуйте, як зберігаються вкладення.
default_storage_bk:
  title: Бекенд файлового сховища
  content: >
    Choose how attachments are stored. <br><br> Additional storage backends can be added by installing storage plugins
max_file_size:
  title: Максимальний розмір файлу
  content: >
    Виберіть максимальний розмір файлів вкладень, які дозволені агентам. Це включає підготовлені вкладення, статті бази знань та вкладення до заявок і відповідей. Верхній ліміт контролюється налаштуваннями PHP <code>upload_max_filesize</code>.
  links:
    - 
      title: Налаштування PHP ini
      href: "http://php.net/manual/en/ini.core.php#ini.upload-max-filesize"
files_req_auth:
  title: Require Login
  content: >
    Enable this setting to forbid serving attachments to unauthenticated users. That is, users must sign into the system (both end users and agents), in order to view attachments. <br><br> From a security perspective, be aware that the user's browser may retain previously-viewed files in its cache. Furthermore, all file links on your helpdesk automatically expire after about 24 hours.

<html>

<head>
    <style type="text/css">
@page {
    header: html_def;
    footer: html_def;
    margin: 15mm;
    margin-top: 30mm;
    margin-bottom: 22mm;
}
.logo {
  max-width: 220px;
  max-height: 71px;
  width: auto;
  height: auto;
  margin: 0;
}
#ticket_thread .message,
#ticket_thread .response,
#ticket_thread .note {
    margin-top:10px;
    border:1px solid #aaa;
    border-bottom:2px solid #aaa;
}
#ticket_thread .header {
    text-align:left;
    border-bottom:1px solid #aaa;
    padding:3px;
    width: 100%;
    table-layout: fixed;
}
#ticket_thread .message .header {
    background:#C3D9FF;
}
#ticket_thread .response .header {
    background:#FFE0B3;
}
#ticket_thread .note .header {
    background:#FFE;
}
#ticket_thread .info {
    padding:5px;
    background: snow;
    border-top: 0.3mm solid #ccc;
}
table.meta-data {
    width: 100%;
}
table.custom-data {
    margin-top: 10px;
}
table.custom-data th {
    width: 25%;
}
table.custom-data th,
table.meta-data th {
    text-align: right;
    background-color: #ddd;
    padding: 3px 8px;
}
table.meta-data td {
    padding: 3px 8px;
}
.faded {
    color:#666;
}
.pull-left {
    float: left;
}
.pull-right {
    float: right;
}
.flush-right {
    text-align: right;
}
.flush-left {
    text-align: left;
}
.ltr {
    direction: ltr;
    unicode-bidi: embed;
}
.headline {
    border-bottom: 2px solid black;
    font-weight: bold;
}
div.hr {
    border-top: 0.2mm solid #bbb;
    margin: 0.5mm 0;
    font-size: 0.0001em;
}
.thread-entry, .thread-body {
    page-break-inside: avoid;
}
<?php include ROOT_DIR . 'css/thread.css'; ?>
    </style>
</head>
<body>

<htmlpageheader name="def" style="display:none">
<!-- <?php //if ($logo = $cfg->getClientLogo()) { ?>
    <img src="cid:<?php //echo $logo->getKey(); ?>" class="logo"/>
<?php //} else { ?>
    <img src="<?php //echo INCLUDE_DIR . 'fpdf/print-logo.png'; ?>" class="logo"/>
<?php //} ?>
    <div class="hr">&nbsp;</div>
    <table><tr>
        <td class="flush-left"><?php //echo (string) $ost->company; ?></td>
        <td class="flush-right"><?php //echo Format::daydatetime(Misc::gmtime()); ?></td>
    </tr></table> -->
</htmlpageheader>

<htmlpagefooter name="def" style="display:none">
    <div class="hr">&nbsp;</div>
    <table width="100%">
    <tr>
      <td class="flush-left">
        Complaint #909090 printed by
        Juan dela Cruz on
        <?php echo Format::daydatetime(Misc::gmtime()); ?>
      </td>
      <td class="flush-right">
          Page {PAGENO}
      </td>
    </tr>
  </table>
</htmlpagefooter>

<!-- Ticket metadata -->
<p style="text-align: center; font-weight: bold;">Republic of the Philippines</p>
<p style="text-align: center; font-weight: bold;">NATIONAL PRIVACY COMMISSION</p>
<p style="text-align: center;">COMPLAINTS-ASSISTED FORM</p>

<p>By filling out this complaint form, you hereby sworn that you are the complainant and you will have to appear
in the office ofr oath taking. Willful and deliberate assertion of falsehood will be criminally prosecuted. List
of accepted government issued ID's: </p>

<ul>
  <li>Philippine Passport</li>
  <li>Philippine Driver's License</li>
  <li>PRC ID</li>
  <li>Postal ID</li>
  <li>Voter's ID</li>
  <li>GSIS Card</li>
  <li>SSS Card</li>
  <li>TIN Card</li>
  <li>Student ID</li>
</ul>

<p>Answer the following questions. o not leave any field blank.</p>

<p>Do you have a concern about a privacy violation, personal data breach or matters related to personal data protection,
  or any other violation of the Data Privacy Act, its IRR and other issuances?</p>

<table style="border:1px solid black" cellpadding="0" cellspacing="0">
  <tbody>

    <tr>
      <td>[checkbox]</td>
      <td>YES. Proceed to the next section.</td>
    </tr>
    <tr>
      <td>[checkbox]</td>
      <td>If NO, do not proceed. The National Privacy Commission may have no power to act on your complaint.
        The Commission can only act on matters that relate to Data Privacy.</td>
    </tr>

    <tr>
      <!-- cellspan  -->
      <td>Does your concern affect you personally or it involves your personal data?</td>
    </tr>
    <tr>
      <td>[checkbox]</td>
      <td>YES, Proceed.</td>
    </tr>
    <tr>
      <td>[checkbox]</td>
      <td>NO.</td>
    </tr>

    <tr>
      <!-- cellspan -->
      <td>Have you contacted the agency/individual you are about to complain?</td>
    </tr>
    <tr>
      <td>[checkbox]</td>
      <td>YES, Please attach proof.</td>
    </tr>
    <tr>
      <td>[checkbox]</td>
      <td>NO. Please explain</td>
    </tr>
    <tr>
      <td>[blank]</td>
      <td>[3 rows]</td>
    </tr>
  </tbody>
</table>

<p>Complainant's Personal Information</p>

<table>
  <tr>
    <td>First Name</td>
    <td>[line]</td>
  </tr>
  <tr>
    <td>Middle Initial</td>
    <td>[line]</td>
  </tr>
  <tr>
    <td>Last Name</td>
    <td>[line]</td>
  </tr>
  <tr>
    <td>Valid ID</td>
    <td>[line]</td>
  </tr>
  <tr>
    <td>ID Number</td>
    <td>[line]</td>
  </tr>
  <tr>
    <td>Present Address</td>
    <td>[line]</td>
  </tr>
  <tr>
    <!-- rowspan 3 rows  -->
    <td>
      *Please indicate complete address, including zip code, for mailing of orders and other issuances of this Commission
    </td>
  </tr>
  <tr>
    <td>Permanent Address</td>
    <td>[line]</td>
  </tr>
  <tr>
    <!-- rowspan 3 rows -->
    <td>*Please indicate complete address, including zip code, for mailing of orders and other issuances of this Commission</td>
  </tr>
  <tr>
    <td>Email Address</td>
    <td>[line]</td>
  </tr>
  <tr>
    <td>Contact Number</td>
    <td>[line]</td>
  </tr>
</table>

<p>Respondent's Infomation (Party Complainted Against)</p>

<table>
  <tr>
    <td>Name</td>
    <td>[line]</td>
  </tr>
  <tr>
    <td>Present Address</td>
    <td>[line]</td>
  </tr>
  <tr>

    <td>*Please indicate complete
address, including zip
code, for mailing of orders
and other issuances of this
Commission</td>
    <td>[line]</td>
  </tr>
  <tr>
    <td>Contact Number</td>
    <td>[line]</td>
  </tr>
  <tr>
    <td>Website (if any)</td>
  </tr>
</table>

<p>Complaint</p>
<table>
  <tr>
    <td>Describe the complaint</td>
    <td></td>
  </tr>
  <tr>
    <td>*Please write legibly</td>
  </tr>
  <tr>  
    <td>Please select the provision violated by the respondent.</td>
  </tr>

  <tr>
    <td>
      <p>Section 25</p>
      <p>
        Unauthorized Processing of Personal Infomation and Sensitive 
        Personal Information
      </p>
    </td>
  </tr>
  <tr>
    <td>
      <p>Section 26</p>
      <p>
        Accessing Personal Information and Sensitive Personal Information
        Due to Negligence
      </p>
    </td>
  </tr>
  <tr>
    <td>
      <p>Section 27</p>
      <p>
        Improper Disposal of Personal Information and Sensitive Personal 
        Information
      </p>
    </td>
  </tr>
  <tr>
    <td>
      <p>Section 28</p>
      <p>
        Processing of Personal Information and Sensitive Personal Information 
        for Unauthorized Purposes
      </p>
    </td>
  </tr>
  <tr>
    <td>
      <p>Section 29</p>
      <p>
        Unauthorized Access or Intentional Breach
      </p>
    </td>
  </tr>
  <tr>
    <td>
      <p>Section 30</p>
      <p>
        Concealmenet of Security Breaches Involving Sensitive Personal Information 
      </p>
    </td>
  </tr>
  <tr>
    <td>
      <p>Section 31</p>
      <p>
        Malicious Disclosure
      </p>
    </td>
  </tr>
  <tr>
    <td>
      <p>Section 32</p>
      <p>
        Unauthorized Disclosure 
      </p>
    </td>
  </tr>
  <tr>
    <td>
      <p>Section 35</p>
      <p>
        Large-Scale
      </p>
    </td>
  </tr>
  <tr>
    <td>
      <p>Section 36</p>
      <p>
        Offense Committed by Public Officer
      </p>
    </td>
  </tr>
</table>

<table>
    <tr>
        <td>What personal information about your was affected?</td>
    </tr>
    <tr>
        <td>Date of Incident</td>
    </tr>
    <tr>
        <td>Time of Incident</td>
    </tr>
    <tr>
        <td>Place of Incident</td>
    </tr>
    <tr>
        <td></td>
    </tr>
>/table>
<h1>Complaint #90909090/h1>
<table class="meta-data" cellpadding="0" cellspacing="0">
<tbody>
<tr>
    <th><?php echo __('Status'); ?></th>
    <td>[enter status here]</td>
    <th><?php echo __('Name'); ?></th>
    <td>[enter name here]</td>
</tr>
<tr>
    <th><?php echo __('Priority'); ?></th>
    <td>[enter priority here]</td>
    <th><?php echo __('Email'); ?></th>
    <td>[enter email here]</td>
</tr>
<tr>
    <th><?php echo __('Department'); ?></th>
    <td>[enter department here]</td>
    <th><?php echo __('Phone'); ?></th>
    <td>[enter phone here]</td>
</tr>
<tr>
    <th><?php echo __('Create Date'); ?></th>
    <td>[enter date here]</td>
    <th><?php echo __('Source'); ?></th>
    <td>[enter source here]</td>
</tr>
</tbody>
<tbody>
    <tr><td colspan="4" class="spacer">&nbsp;</td></tr>
</tbody>
<tbody>
<tr>
    <th><?php echo __('Assigned To'); ?></th>
    <td>[enter assignee here]</td>
    <th><?php echo __('Help Topic'); ?></th>
    <td>[enter help topic here]</td>
</tr>
<tr>
    <th><?php echo __('SLA Plan'); ?></th>
    <td>[enter sla plan here]</td>
    <th><?php echo __('Last Response'); ?></th>
    <td>[enter last response here]</td>
</tr>
<tr>
    <th><?php echo __('Due Date'); ?></th>
    <td>[enter due date here]</td>
    <th><?php echo __('Last Message'); ?></th>
    <td>[enter last message here]</td>
</tr>
</tbody>
</table>



</body>
</html>
